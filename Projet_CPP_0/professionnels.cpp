#include "professionnels.h"
#include "exception.h"


Professionnels::Professionnels(string firstname, string lastname, int id, char sex, int numero, string rue, string complement, int codePostal, string ville, string company, string email)
    :Contact(firstname, lastname, id, sex)
{
    this->Setadresse(numero,rue);
    this->Setcomplement(complement);
    this->SetcodePostal(codePostal);
    this->Setville(ville);
    this->Setemail(email);
    this->Setcompany(company);
}

Professionnels::~Professionnels()
{
    cout << "destruciotn professionnels" << endl;
}

void Professionnels::Setcompany(string vo)
{
    if (vo.length() > LG_COMPANY)
    {
        this->company = "empty";
        throw Exception(Erreurs::ERR_COMPANY);
    }
    else
    {
        for(int i =0; i< vo.length(); i++)
        {
            vo[i] = toupper(vo[i]);
        }
        this->company = vo;
    }
}

void Professionnels::Setemail(string vo)
{
    if(vo.find('@')!=std::string::npos)
    {
        email = vo;
    }
    else

    {
        throw Exception(Erreurs::ERR_EMAIL);
    }
}

void Professionnels::Setadresse(int num, string rue)
{
    this->adresse = to_string(num) + ", " + rue ;
}



void Professionnels::Setville(string vo)
{
    for(int i =0; i< vo.length(); i++)
    {
        vo[i] = toupper(vo[i]);
    }
    this->ville = vo;
}

