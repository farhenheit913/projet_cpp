#ifndef PRIVES_H
#define PRIVES_H

#include "contact.h"

#include <iostream>
#include <cstring>

using namespace std;

class Prives : public Contact
{
    public:
        Prives(string, string , int , char , int , string, string, int , string, int, int, int);
        virtual ~Prives();

        int Getnum() { return num; }
        void Setnum(int val) { num = val; }

        string Getrue() { return rue; }
        void Setrue(string val) { rue = val; }

        string Getcomplement() { return complement; }
        void Setcomplement(string val) { complement = val; }

        int GetcodePostal() { return codePostal; }
        void SetcodePostal(int val) { codePostal = val; }

        string Getville() { return ville; }
        void Setville(string);
        string GetdateNaissance() { return dateNaissance; }
        void SetdateNaissance(int ,int, int) ;

        string Getadresse() { return adresse; }
        void Setadresse(int, string);

    protected:

    private:

        string dateNaissance;
        string adresse;

        int num;
        string rue;
        string complement;
        int codePostal;
        string ville;
};

#endif // PRIVES_H
