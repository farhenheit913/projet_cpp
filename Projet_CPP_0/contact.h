#ifndef CONTACT_H
#define CONTACT_H
#include <iostream>
#include <cstring>
using namespace std;


#define LG_NAME 30


class Contact
{
    public:
        Contact(string, string, int, char );
        virtual ~Contact();

        int GetId() { return Id; }
        void SetId(int val) { Id = val; }

        string GetFirstName() { return FirstName; }
        void SetFirstName(string);

        string GetLastName() { return LastName; }
        void SetLastName(string);

        char GetSex() { return Sex; }
        void SetSex(char);

    protected:

    private:
        int Id;
        string FirstName;
        string LastName;
        char Sex;
};
#endif // CONTACT_H
