#ifndef PROFESSIONNELS_H
#define PROFESSIONNELS_H

#include "contact.h"

#include <iostream>
#include <cstring>

#define LG_COMPANY 50

class Professionnels : public Contact
{
    public:
        Professionnels(string, string , int , char , int , string, string, int , string, string, string );
        virtual ~Professionnels();

        string Getcompany() { return company; }
        void Setcompany(string);

        string Getemail() { return email; }
        void Setemail(string);


        string Getadresse() { return adresse; }
        void Setadresse(int, string);

        string Getcomplement() { return complement; }
        void Setcomplement(string val) { complement = val; }

        int GetcodePostal() { return codePostal; }
        void SetcodePostal(int val) { codePostal = val; }

        string Getville() { return ville; }
        void Setville(string) ;
    protected:

    private:
        string company;
        string email;
        string adresse;
        string complement;
        int codePostal;
        string ville;
};

#endif // PROFESSIONNELS_H
