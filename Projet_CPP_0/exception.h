#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <iostream>
#include <stdexcept>

using namespace std;

enum class Erreurs {ERR_PRENOM, ERR_NOM, ERR_SEX, ERR_ANNEE, ERR_COMPANY, ERR_EMAIL, ERR_AUTRES};

class Exception: exception
{

    private:
        Erreurs codeErreur;
        mutable string message;

    public:

        Exception(Erreurs) throw();
        virtual ~Exception() throw();

        const char * what() const throw() override;

        string Getmessage() const;


};

#endif // EXCEPTION_H
