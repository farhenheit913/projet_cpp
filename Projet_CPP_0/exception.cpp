#include "exception.h"

Exception::Exception(Erreurs e) throw()
{
    this->codeErreur = e;
}

Exception::~Exception() throw()
{
    cout << "Destruction Erreur " << endl;
}

const char* Exception::what() const throw()
{
    this->Getmessage();
    return message.c_str();
}

string Exception::Getmessage() const
{
    switch (this->codeErreur)
    {
        case Erreurs::ERR_PRENOM:
            message = "Format Prenom trop long";
            break;
        case Erreurs::ERR_NOM:
            message = "Format Nom trop long";
            break;
        case Erreurs::ERR_SEX:
            message = "Format Sex incorrect";
            break;
        case Erreurs::ERR_ANNEE:
            message = "Format annee incorrect";
            break;
        case Erreurs::ERR_COMPANY:
            message = "Format company trop long";
            break;
        case Erreurs::ERR_EMAIL:
            message = "Format email incorrect";
            break;
        default:
            message = "Autre Erreur !";
            break;
    }
    return this->message;
}
