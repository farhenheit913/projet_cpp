#include <iostream>
#include "contact.h"
#include "exception.h"
#include "professionnels.h"
#include "prives.h"

using namespace std;

int main()
{
    try
    {
        Contact c1("jeAN","DUPond",1235411,'H');
        cout << "Prenom : " << c1.GetFirstName() << endl;
        cout << "Nom de famille : " << c1.GetLastName() << endl;
        cout << "ID : " << c1.GetId() << endl;
        cout << "Sex : " << c1.GetSex() << endl;

    }
    catch(const Exception& ex)
    {
        cout << ex.what() << endl;
    }
    catch(...)
    {
        cout << "Autre Erreur !!!" << endl;

    }


    cout<< "----------" << endl;

    try
    {
        Prives p2("jeAN","DUPond",1235411,'H', 12, "boulevard de la liberte", "Complement", 91400, "Paris", 12,01,1998);


        cout << "Prenom : " << p2.GetFirstName() << endl;
        cout << "Nom de famille : " << p2.GetLastName() << endl;
        cout << "ID : " << p2.GetId() << endl;
        cout << "Sex : " << p2.GetSex() << endl;
        cout << "Rue : " << p2.Getadresse() << endl;
        cout << "date : " << p2.GetdateNaissance() << endl;
        cout << "Complement : " << p2.Getcomplement() << endl;

    }
    catch(const Exception& ex)
    {
        cout << ex.what() << endl;
    }
    catch(...)
    {
        cout << "Autre Erreur !!!" << endl;
    }

    cout<< "----------" << endl;


    try
    {
        Professionnels p1("jeAN","DUPond", 1235411,'H', 5, "rue Nationale", "Complement 2", 91400, "Lille",  "Entreprise X", "email@email");

        cout << "Prenom : " << p1.GetFirstName() << endl;
        cout << "Nom de famille : " << p1.GetLastName() << endl;
        cout << "ID : " << p1.GetId() << endl;
        cout << "Sex : " << p1.GetSex() << endl;
        cout << "Rue : " << p1.Getadresse() << endl;
        cout << "email : " << p1.Getemail() << endl;
        cout << "Company : " << p1.Getcompany() << endl;
        cout << "Complement : " << p1.Getcomplement() << endl;
        cout << "ville : " << p1.Getville() << endl;


    }
    catch(const Exception& ex)
    {
        cout << ex.what() << endl;
    }
    catch(...)
    {
        cout << "Autre Erreur !!!" << endl;
    }



}
